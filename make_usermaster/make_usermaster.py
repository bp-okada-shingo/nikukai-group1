
# coding: utf-8

# In[119]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm_notebook as tqdm
import datetime
import seaborn as sns
import collections
import statistics


# In[120]:


df = pd.read_csv('201805311600_確率*10_20_30倍+スコアつき（元）.csv')


# In[121]:


df['sakusei_date'].max()


# In[122]:


df.columns


# ## ユーザーマスター作成

# In[123]:


df01 = df.loc[:,['first', 'first_syozoku_1', 'first_syozoku_2', 'first_syozoku_3','first_syozoku_kakou',
                 'first_sei' ,'first_nyuusya', 'first_yakusyoku','first_yakusyoku_kakou', 'first_zaiseki', 'first_shinsotsu']]
df02 = df.loc[:,['second',  'second_syozoku_1', 'second_syozoku_2', 'second_syozoku_3', 'second_syozoku_kakou',
                 'second_sei','second_nyuusya','second_yakusyoku', 'second_yakusyoku_kakou','second_zaiseki', 'second_shinsotsu']]
df03 = df.loc[:,['third', 'third_syozoku_1', 'third_syozoku_2', 'third_syozoku_3',  'third_syozoku_kakou',
                 'third_sei','third_nyuusya','third_yakusyoku', 'third_yakusyoku_kakou','third_zaiseki','third_shinsotsu']]
df01.columns = ['person', 'syozoku_1', 'syozoku_2', 'syozoku_3', 'syozoku_kakou', 'sei' ,'nyuusya', 'yakusyoku','yakusyoku_kakou', 'zaiseki','shinsotsu']
df02.columns = ['person', 'syozoku_1', 'syozoku_2', 'syozoku_3', 'syozoku_kakou', 'sei' ,'nyuusya', 'yakusyoku','yakusyoku_kakou', 'zaiseki','shinsotsu']
df03.columns = ['person', 'syozoku_1', 'syozoku_2', 'syozoku_3', 'syozoku_kakou', 'sei' ,'nyuusya', 'yakusyoku','yakusyoku_kakou', 'zaiseki','shinsotsu']


# In[124]:


print(df01.shape,df02.shape,df02.shape)


# In[125]:


df03.head()


# In[126]:


df_person = df01
df_person = df_person.append(df02, ignore_index=True)
df_person = df_person.append(df03, ignore_index=True)


# In[127]:


df_person.head()


# In[128]:


df_person.shape


# In[129]:


df_person = df_person[~df_person.duplicated()]


# In[130]:


df_person.shape


# In[131]:


df_person.to_csv('df_person.csv')


# ## flagを用意する

# In[132]:


df1 = df.loc[:,['first', 'first_status', 'haishin_flag', 'match_flag', 'zissi_flag', ]]
df2 = df.loc[:,['second','second_status', 'haishin_flag', 'match_flag', 'zissi_flag', ]]
df3 = df.loc[:,['third', 'third_status','haishin_flag', 'match_flag', 'zissi_flag', ]]
df1.columns = ['person', 'status','haishin_flag','match_flag','zissi_flag']
df2.columns = ['person', 'status','haishin_flag','match_flag','zissi_flag']
df3.columns = ['person', 'status','haishin_flag','match_flag','zissi_flag']


# In[133]:


df_person_flag = df1
df_person_flag = df_person_flag.append(df2, ignore_index=True)
df_person_flag = df_person_flag.append(df3, ignore_index=True)


# ## ダミー化してあげる

# In[134]:


df_status = pd.get_dummies(df_person_flag['status'])


# In[135]:


df_status.head()


# In[136]:


df_status.columns = ['status0', 'status1', 'status2']


# In[137]:


df_status.sum()


# In[138]:


df_status.shape


# ## ダミー化したものをくっつける

# In[139]:


del df_person_flag['status']


# In[140]:


df_person_flag.head()


# In[141]:


df_person_flag = pd.merge(df_person_flag, df_status, left_index=True, right_index=True)


# In[142]:


df_person_flag.head()


# In[143]:


df_person_flag.shape


# ## 集計

# In[144]:


df_result = df_person_flag.groupby(['person']).sum()


# In[145]:


df_result.shape


# In[146]:


df_result.head()


# In[147]:


df_result['match/haishin'] = df_result['match_flag']/df_result['haishin_flag']
df_result['zissi/haishin'] = df_result['zissi_flag']/df_result['haishin_flag']
df_result['zissi/match'] = df_result['zissi_flag']/df_result['match_flag']
df_result['(s1+s2)/haishin'] = (df_result['status1']+df_result['status2'])/df_result['haishin_flag']
df_result['(s1)/haishin'] = df_result['status1']/df_result['haishin_flag']
df_result['(s2)/haishin'] = df_result['status2']/df_result['haishin_flag']
df_result['zissi/s1'] = df_result['zissi_flag']/df_result['status1']


# In[148]:


df_result.head()


# In[149]:


df_result = df_result.reset_index(drop=False)


# ## くっつけたい

# In[150]:


df_ans = pd.merge(df_person, df_result, on = 'person',how='left')


# In[151]:


df_ans.head()


# ## クラスタリングの結果と合わせる

# In[152]:


cl = pd.read_csv('add_cluster.csv')


# In[153]:


df_return = pd.merge(cl, df_ans, on = 'person',how='left')


# In[154]:


df_return.head()


# In[155]:


df_return = df_return.drop("Unnamed: 0", axis=1)


# In[156]:


df_return.head()


# In[157]:


df_return.columns


# In[159]:


df_return.to_csv('20180604_member.csv')


# ## 入社からの経過日数を入れる

# In[162]:


df = pd.read_csv('20180604_member.csv', parse_dates = ['nyuusya'])


# In[165]:


df.shape


# In[166]:


df.head()


# In[167]:


df['nyuusya'] = df['nyuusya'].fillna(datetime.datetime(1900, 1, 1))


# In[168]:


time = datetime.datetime(2018, 3, 1)
df['keika'] = [time - i for i in df['nyuusya']]


# In[169]:


df.head(20)


# In[170]:


df['keika'].max()


# In[183]:


df.shape


# In[171]:


df = df.drop("Unnamed: 0", axis=1)


# In[182]:


df.to_csv('20180604_member.csv')


# ## column名を直す

# In[181]:


df.columns


# In[180]:


df.columns = ['id',
              'cluster_no', 
              'syozoku_1', 
              'syozoku_2', 
              'syozoku_3',
              'syozoku_kakou',
              'sei', 
              'nyuusya', 
              'yakusyoku', 
              'yakusyoku_kakou',
              'zaiseki', 'shinsotsu',
              'kako_count', 
              'kako_match', 
              'kako_zissi',
              'kako_mihenshin', 
              'kako_sanka',
              'kako_fusanka',
              'match/count', 
              'zissi/count',
              'zissi/match', 
              'henshin/count', 
              'sanka/count', 
              'fusanka/count',
              'zissi/sanka', 
              'keika']


# In[184]:


df['keika']=list(df['keika'].apply(lambda x: x.days))


# In[186]:


df.head()


# In[187]:


df.to_csv('20180604_member.csv')


# In[188]:


df.columns


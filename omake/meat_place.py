
# coding: utf-8

import pandas as pd
import numpy as np
from tqdm import tqdm_notebook as tqdm
import datetime
import statistics
import math
import re

#データの読み込み三井さんからの経理データをエクセルで少し綺麗にしたもの
df = pd.read_csv('Book1.csv', parse_dates = ['date'])


#金額がマイナスなので−１をかける
#店名が()の前に入っていたので、それを切り分けて置いておく
df['year'] = [i.year for i in df['date']]
df['month'] = [i.month for i in df['date']]
df['new_money'] = [i for i in df['money']*-1]
df['shop_name'] = [re.split('（', i)[0] for i in df['name']]


#店名に入っている半角全角スペースをなくし、綺麗にする
#店名のアルファベットを全て大文字にする
df['shop_name'] = [i.replace(' ', '').upper() for i in df['shop_name']]

#グルーピングして集計する
df['count'] = 1
df1 = df.loc[:, ['shop_name', 'count', 'new_money']]
df1 = df1.groupby(['shop_name']).sum()


#肉会が開催された順で並びかえる
df1 = df1.sort_values(by='count', ascending=False)

#肉会での使用金額の総計の算出
sum(df['new_money'])

#csvとして保存
df1.to_csv('df1.csv')


# coding: utf-8

# In[168]:


import pandas as pd
import numpy as np
from tqdm import tqdm_notebook as tqdm
import matplotlib
import matplotlib.pyplot as plt
import collections
import statistics
import random


# In[169]:


df = pd.read_csv('meetup.csv', parse_dates = ['zissi_date','shikkou_date','sakusei_date','match_date','reply_deadline'])


# In[170]:


df.tail()


# In[171]:


df.columns


# In[172]:


df['sakusei_year'] = [df['sakusei_date'][i].year for i in range(len(df))]


# In[173]:


df['sakusei_month'] = [df['sakusei_date'][i].month for i in range(len(df))]


# In[174]:


#sakuseidateごとにペアのマッチング率を計算する
df = df[df['sakusei_year'] <= 2018]


# In[175]:


df_pairs = pd.DataFrame(columns=[ 'person1'])
df01 = df.loc[:,['first']]
df02 = df.loc[:,['second']]
df03 = df.loc[:,['third']]
df01.columns = ['person1']
df02.columns = ['person1']
df03.columns = ['person1']
df_pairs = df_pairs.append(df01, ignore_index=True)
df_pairs = df_pairs.append(df02, ignore_index=True)
df_pairs = df_pairs.append(df03, ignore_index=True)


# In[176]:


person2018 = df_pairs.nunique()


# ## 実施フラグがTRUEのものだけにする

# In[177]:


df1 = df[df.zissi_flag == True]


# In[178]:


df1['zissi_flag'].head()


# In[179]:


print(df.shape, df1.shape)


# In[180]:


sakusei2018 = len(df)
zissi2018 = len(df1)


# ## ペアを数えたい

# ### データフレーム作成

# In[181]:


df_pairs = pd.DataFrame(columns=['nikukai_id', 'person1', 'person2'])


# In[182]:


df01 = df1.loc[:,['nikukai_id','first', 'second']]
df02 = df1.loc[:,['nikukai_id','second', 'third']]
df03 = df1.loc[:,['nikukai_id','first', 'third']]
df01.columns = ['nikukai_id', 'person1', 'person2']
df02.columns = ['nikukai_id', 'person1', 'person2']
df03.columns = ['nikukai_id', 'person1', 'person2']


# In[183]:


df_pairs = df_pairs.append(df01, ignore_index=True)
df_pairs = df_pairs.append(df02, ignore_index=True)
df_pairs = df_pairs.append(df03, ignore_index=True)


# In[184]:


df_pairs.shape


# ### 右がID大きいようにする

# In[185]:


for i in tqdm(range(len(df_pairs))):
    if df_pairs['person2'][i] < df_pairs['person1'][i]:
        j = df_pairs['person1'][i]
        k = df_pairs['person2'][i]
        df_pairs['person2'][i] = j
        df_pairs['person1'][i] = k


# In[186]:


df_pairs.head()


# In[187]:


df_result = df_pairs.groupby(['person1','person2']).count()


# In[188]:


type(df_result)


# In[189]:


df_result.head()


# In[190]:


df_result.to_csv('pair_match.csv')


# ## 回数ごとに調査

# In[191]:


l = df_result.nikukai_id
c = collections.Counter(l)


# In[192]:


print(c)


# In[193]:


count2018 = c


# In[194]:


#従業員数（退社した人も含む）
df_1 = df_pairs['person1']
df_1 = df_1.append(df_pairs['person2'], ignore_index=True)


# In[195]:


print(person2014[0], sakusei2014, zissi2014, count2014, sum([count2014[i+2]*(i+1) for i in range(5)])/(zissi2014*3))
print(person2015[0], sakusei2015, zissi2015, count2015, sum([count2015[i+2]*(i+1) for i in range(5)])/(zissi2015*3))
print(person2016[0], sakusei2016, zissi2016, count2016, sum([count2016[i+2]*(i+1) for i in range(5)])/(zissi2016*3))
print(person2017[0], sakusei2017, zissi2017, count2017, sum([count2017[i+2]*(i+1) for i in range(5)])/(zissi2017*3))
print(person2018[0], sakusei2018, zissi2018, count2018, sum([count2018[i+2]*(i+1) for i in range(5)])/(zissi2018*3))


# ## 3人全く一緒ってあるのかな

# ### 必要なデータをもってくる

# In[30]:


df20 = df1.loc[:,['nikukai_id','first', 'second','third']]


# In[31]:


df20.head()


# In[32]:


df20 = df20.reset_index(drop=True)


# In[33]:


for i in tqdm(range(len(df20))):
    j = df20['first'][i]
    k = df20['second'][i]
    l = df20['third'][i]
    df20['first'][i] = min([j,k,l])
    df20['second'][i] = statistics.median([j,k,l])
    df20['third'][i] = max([j,k,l])


# In[34]:


df20.shape


# In[59]:


df20.head()


# In[62]:


df20_result = df20.groupby(['first', 'second', 'third']).count()


# In[64]:


df20_result.max()


# In[65]:


l = df20_result.nikukai_id
c = collections.Counter(l)


# In[66]:


print(c)


# In[778]:


s = range(210)
random.seed(0)


# In[779]:


random.sample(s, 3)


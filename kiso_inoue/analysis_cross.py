
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm_notebook as tqdm
import datetime
import seaborn as sns
import collections
import statistics


# In[2]:


df = pd.read_csv('20180604_member.csv', parse_dates = ['nyuusya'], index_col=0)


# In[116]:


df.head(15)


# In[9]:


df.columns


# In[25]:


df.shape


# In[3]:


def keika_flag(i):
    #経過日数ごとにフラグをつける
    if i == 43158:
        return('F')
    elif i < 365*3:
        return('C')
    elif i < 365*5:
        return('B')
    else:
        return('A')
        


# In[4]:


df['keika_flag'] = df['keika'].apply(keika_flag)


# ## 各部署ごとの集計を行う

# In[5]:


df_honbu = df.loc[:, ['syozoku_1', 'sei', 'yakusyoku_kakou', 'shinsotsu', 'zaiseki', 'keika_flag']]


# In[6]:


df_honbu[['syozoku_1','zaiseki','shinsotsu']] = df_honbu[['syozoku_1','zaiseki','shinsotsu']].fillna('退社済')


# In[119]:


df_honbu.head()


# In[120]:


pd.crosstab(df_honbu['syozoku_1'], df_honbu['sei'],  margins=True)


# In[39]:


pd.crosstab(df_honbu['syozoku_1'], df_honbu['shinsotsu'],  margins=True)


# In[40]:


pd.crosstab(df_honbu['syozoku_1'], df_honbu['zaiseki'],  margins=True)


# In[47]:


pd.crosstab(df_honbu['syozoku_1'], df_honbu['yakusyoku_kakou'],  margins=True)


# In[121]:


pd.crosstab(df_honbu['syozoku_1'], df_honbu['keika_flag'],  margins=True)


# In[36]:


pd.crosstab(df_honbu['syozoku_1'], [df_honbu['sei'], df_honbu['shinsotsu']],  margins=True)


# ## クラスターごとの集計を行う

# In[7]:


df_honbu = df_honbu.groupby(['syozoku_1']).count()


# In[8]:


df_cluster = df.loc[:, ['cluster_no', 'syozoku_1', 'sei', 'yakusyoku_kakou', 'shinsotsu', 'zaiseki', 'keika_flag']]


# In[129]:


df_cluster[['syozoku_1','zaiseki','shinsotsu']] = df_cluster[['syozoku_1','zaiseki','shinsotsu']].fillna('退社済')


# In[43]:


df_cluster.head()


# In[44]:


pd.crosstab(df_cluster['cluster_no'], df_cluster['sei'],  margins=True)


# In[48]:


pd.crosstab(df_cluster['cluster_no'], df_cluster['shinsotsu'],  margins=True)


# In[49]:


pd.crosstab(df_cluster['cluster_no'], df_cluster['zaiseki'],  margins=True)


# In[50]:


pd.crosstab(df_cluster['cluster_no'], df_cluster['syozoku_1'],  margins=True)


# In[124]:


pd.crosstab(df_cluster['cluster_no'], df_cluster['keika_flag'],  margins=True)


# In[130]:


pd.crosstab([df_cluster['cluster_no'], df_cluster['syozoku_1']], df_cluster['keika_flag'],  margins=True)


# ## 在籍期間について(退社した人は除く)

# In[9]:


df_zaiseki = df


# In[10]:


#経過日数＝43158の人は退社済みなので削除
df_zaiseki = df_zaiseki[df_zaiseki['keika'] != 43158]


# In[53]:


df_zaiseki.head()


# In[70]:


df_zaiseki_1 = df_zaiseki.loc[:, ['cluster_no', 'syozoku_1', 'keika']]


# In[71]:


df_zaiseki_1.head()


# In[75]:


df_zaiseki_1.groupby(['cluster_no', 'syozoku_1']).count()


# In[69]:


df_zaiseki['keika'].mean()


# In[15]:


pd.crosstab(df['keika_flag'], df['yakusyoku_kakou'],  margins=True)


# ## 様々な指標についてのまとめ

# In[12]:


df1 = df
df1[['syozoku_1','zaiseki','shinsotsu']] = df1[['syozoku_1','zaiseki','shinsotsu']].fillna('退社済')
df1.groupby([ 'keika_flag']).var()



# coding: utf-8

# In[31]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm_notebook as tqdm
import datetime
import seaborn as sns
import collections
import statistics


# In[2]:


df = pd.read_csv('meetup.csv', parse_dates = ['zissi_date', 'shikkou_date','sakusei_date','match_date','reply_deadline'])


# In[3]:


df.columns


# In[4]:


df.head()


# In[7]:


print(df['first_syozoku_1'][6000], df['first_syozoku_2'][6000], df['first_syozoku_3'][6000])


# ## どの本部に何人所属しているのか

# In[9]:


l = df.first_syozoku_1
collections.Counter(l)


# ## 必要な情報だけ取り出して、本部NaNを消す

# In[20]:


df1 = df.loc[:, ['nikukai_id','haishin_flag','match_flag','zissi_flag', 'first_syozoku_1','second_syozoku_1','third_syozoku_1']]


# In[21]:


df1.shape


# In[23]:


df1 = df1.dropna(subset=['first_syozoku_1','second_syozoku_1','third_syozoku_1'])


# In[24]:


df1.head()


# In[25]:


df1.shape


# In[27]:


df1 = df1.reset_index(drop=True)


# ## 3部署の組合せで数える

# In[28]:


i = 0
j = df1['first_syozoku_1'][i]
k = df1['second_syozoku_1'][i]
l = df1['third_syozoku_1'][i]


# In[29]:


max([j, k ,l])


# In[30]:


min([j, k ,l])


# In[32]:


statistics.median([j,k,l])


# In[33]:


for i in tqdm(range(len(df1))):
    j = df1['first_syozoku_1'][i]
    k = df1['second_syozoku_1'][i]
    l = df1['third_syozoku_1'][i]
    df1['first_syozoku_1'][i] = min([j,k,l])
    df1['second_syozoku_1'][i] = statistics.median([j,k,l])
    df1['third_syozoku_1'][i] = max([j,k,l])


# In[34]:


df1.head()


# In[37]:


df1_result = df1.groupby(['first_syozoku_1', 'second_syozoku_1', 'third_syozoku_1']).sum()


# In[60]:


df1_result


# In[59]:


df1_result['match/haishin'] = df1_result['match_flag']/df1_result['haishin_flag']
df1_result['zissi/haishin'] = df1_result['zissi_flag']/df1_result['haishin_flag']
df1_result['zissi/match'] = df1_result['zissi_flag']/df1_result['match_flag']


# In[61]:


df1_result.to_csv('busyo1.csv')


# ## 2部署の組合せで数える

# In[47]:


df_pairs = pd.DataFrame(columns=['nikukai_id', 'person1', 'person2','haishin_flag','match_flag','zissi_flag'])


# In[48]:


df01 = df1.loc[:,['nikukai_id','first_syozoku_1', 'second_syozoku_1','haishin_flag','match_flag','zissi_flag']]
df02 = df1.loc[:,['nikukai_id','second_syozoku_1', 'third_syozoku_1','haishin_flag','match_flag','zissi_flag']]
df03 = df1.loc[:,['nikukai_id','first_syozoku_1', 'third_syozoku_1','haishin_flag','match_flag','zissi_flag']]
df01.columns = ['nikukai_id', 'person1', 'person2','haishin_flag','match_flag','zissi_flag']
df02.columns = ['nikukai_id', 'person1', 'person2','haishin_flag','match_flag','zissi_flag']
df03.columns = ['nikukai_id', 'person1', 'person2','haishin_flag','match_flag','zissi_flag']


# In[49]:


df_pairs = df_pairs.append(df01, ignore_index=True)
df_pairs = df_pairs.append(df02, ignore_index=True)
df_pairs = df_pairs.append(df03, ignore_index=True)


# In[50]:


df_pairs.shape


# In[51]:


df_pairs.tail()


# In[52]:


df_result = df_pairs.groupby(['person1','person2']).sum()


# In[63]:


df_result


# In[62]:


df_result['match/haishin'] = df_result['match_flag']/df_result['haishin_flag']
df_result['zissi/haishin'] = df_result['zissi_flag']/df_result['haishin_flag']
df_result['zissi/match'] = df_result['zissi_flag']/df_result['match_flag']


# In[64]:


df_r = df.reset_index(drop=True)



# coding: utf-8

# In[3]:
＃祝日と平日の各指標の比較計算

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm_notebook as tqdm
import datetime
import seaborn as sns


# In[2]:


df = pd.read_csv('meetup_new.csv', parse_dates = ['zissi_date', 'shikkou_date','sakusei_date','match_date','reply_deadline'])


# In[6]:


df.columns


# ## 使用するデータだけ取り出す

# In[8]:


df1 = df.loc[:,['zissi_flag', 'match_flag','holiday']]


# In[9]:


df01 = df1[df1.holiday == 1]


# In[13]:


df02 = df1[df.holiday != 1]


# In[11]:


df01.head()


# In[14]:


df02.head()


# ## データを分析する

# In[16]:


print('祝日にメールが配信された数:{}　で、マッチ回数：{} で、実施回数：{}'.format(len(df01), df01['match_flag'].sum(), df01['zissi_flag'].sum()) )


# In[18]:


print('祝日にメールが配信され、マッチまでの率：{} で、実施の率：{}'.format(df01['match_flag'].sum()/len(df01), df01['zissi_flag'].sum()/len(df01)) )


# In[19]:


print('祝日にメールが配信され、マッチから実施の率：{}'.format( df01['zissi_flag'].sum()/df01['match_flag'].sum()) )


# In[17]:


print('平日にメールが配信された数:{}　で、マッチ回数：{} で、実施回数：{}'.format(len(df02), df02['match_flag'].sum(), df02['zissi_flag'].sum()) )


# In[21]:


print('平日にメールが配信され、マッチまでの率：{} で、実施の率：{}'.format(df02['match_flag'].sum()/len(df02), df02['zissi_flag'].sum()/len(df02)) )


# In[22]:


print('平日にメールが配信され、マッチから実施の率：{}'.format( df02['zissi_flag'].sum()/df02['match_flag'].sum()) )

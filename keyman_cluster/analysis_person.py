
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm_notebook as tqdm
import datetime
import seaborn as sns
import collections
import statistics


# ## データを入れる

# In[2]:


df = pd.read_csv('meetup.csv', parse_dates = ['zissi_date', 'shikkou_date','sakusei_date','match_date','reply_deadline'])


# In[3]:


df.head()


# In[4]:


df.columns


# In[5]:


df.shape


# ## userデータの復元

# In[6]:


df01 = df.loc[:,['first', 'first_syozoku_1', 'first_syozoku_2', 'first_syozoku_3', 'first_sei' ,'first_nyuusya', 'first_yakusyoku','first_shinsotsu']]
df02 = df.loc[:,['second',  'second_syozoku_1', 
                'second_syozoku_2', 'second_syozoku_3', 'second_sei','second_nyuusya','second_yakusyoku','second_shinsotsu']]
df03 = df.loc[:,['third', 'third_syozoku_1', 'third_syozoku_2', 'third_syozoku_3', 'third_sei','third_nyuusya','third_yakusyoku','third_shinsotsu']]
df01.columns = ['person', 'syozoku_1', 'syozoku_2', 'syozoku_3', 'sei' ,'nyuusya', 'yakusyoku','shinsotsu']
df02.columns = ['person', 'syozoku_1', 'syozoku_2', 'syozoku_3', 'sei' ,'nyuusya', 'yakusyoku','shinsotsu']
df03.columns = ['person', 'syozoku_1', 'syozoku_2', 'syozoku_3', 'sei' ,'nyuusya', 'yakusyoku','shinsotsu']


# In[7]:


df_person = df01
df_person = df_person.append(df02, ignore_index=True)
df_person = df_person.append(df03, ignore_index=True)


# In[8]:


df_person.head()


# In[9]:


df_person.shape


# In[10]:


df_person = df_person[~df_person.duplicated()]


# In[11]:


df_person.shape


# In[12]:


df_person.to_csv('df_person.csv')


# In[13]:


df_person


# ## flagを用意する

# In[14]:


df1 = df.loc[:,['first', 'first_status', 'haishin_flag', 'match_flag', 'zissi_flag', ]]
df2 = df.loc[:,['second','second_status', 'haishin_flag', 'match_flag', 'zissi_flag', ]]
df3 = df.loc[:,['third', 'third_status','haishin_flag', 'match_flag', 'zissi_flag', ]]
df1.columns = ['person', 'status','haishin_flag','match_flag','zissi_flag']
df2.columns = ['person', 'status','haishin_flag','match_flag','zissi_flag']
df3.columns = ['person', 'status','haishin_flag','match_flag','zissi_flag']


# In[15]:


df_person_flag = df1
df_person_flag = df_person_flag.append(df2, ignore_index=True)
df_person_flag = df_person_flag.append(df3, ignore_index=True)


# ## ダミー化してあげる

# In[16]:


df_status = pd.get_dummies(df_person_flag['status'])


# In[17]:


df_status.head()


# In[18]:


df_status.columns = ['status0', 'status1', 'status2']


# In[19]:


df_status.sum()


# In[20]:


df_status.shape


# ## ダミー化したものをくっつける

# In[21]:


del df_person_flag['status']


# In[22]:


df_person_flag.head()


# In[23]:


df_person_flag = pd.merge(df_person_flag, df_status, left_index=True, right_index=True)


# In[24]:


df_person_flag.head()


# In[25]:


df_person_flag.shape


# ## 集計

# In[26]:


df_result = df_person_flag.groupby(['person']).sum()


# In[27]:


df_result.shape


# In[28]:


df_result.head()


# In[29]:


df_result['match/haishin'] = df_result['match_flag']/df_result['haishin_flag']
df_result['zissi/haishin'] = df_result['zissi_flag']/df_result['haishin_flag']
df_result['zissi/match'] = df_result['zissi_flag']/df_result['match_flag']
df_result['(s1+s2)/haishin'] = (df_result['status1']+df_result['status2'])/df_result['haishin_flag']
df_result['(s1)/haishin'] = df_result['status1']/df_result['haishin_flag']
df_result['(s2)/haishin'] = df_result['status2']/df_result['haishin_flag']
df_result['zissi/s1'] = df_result['zissi_flag']/df_result['status1']


# In[35]:


df_result.head()


# In[34]:


df_result = df_result.reset_index(drop=False)


# ## くっつける

# In[36]:


df_ans = pd.merge(df_result, df_person, on = 'person',how='left')


# In[37]:


df_ans.head()


# ## csvにはきだす

# In[38]:


df_ans.to_csv('person.csv')


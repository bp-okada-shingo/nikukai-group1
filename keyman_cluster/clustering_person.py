
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm_notebook as tqdm
import datetime
import seaborn as sns
import collections
import statistics


# In[2]:


from scipy.cluster.hierarchy import linkage, dendrogram, fcluster
from sklearn.cluster import KMeans


# In[3]:


df = pd.read_csv('person.csv')


# In[4]:


df.head(10)


# In[5]:


df.shape


# In[6]:


df.columns


# ## 必要な情報を抜きだす

# In[7]:


df1 = df.loc[:,['Unnamed: 0', 'match/haishin', 'zissi/haishin', 'zissi/match', '(s1+s2)/haishin', '(s1)/haishin']]


# In[8]:


df1.head()


# In[9]:


df1 = df1.fillna(0)


# ## やってみた（階層型クラスタリング　ward ユークリッド距離）

# In[10]:


X_df = df1.drop('Unnamed: 0', axis = 1)
# 階層クラスタリングの実施
linkage_result = linkage(X_df, method='ward', metric='euclidean')
# 1クラスタとする距離の閾値を設定
threshold = 0.175 * np.max(linkage_result[:,2])

# クラスタリング結果の値を取得
clustered = fcluster(linkage_result, threshold, criterion='distance')

# pylabインタフェースでデンドログラムを表示
plt.figure(figsize=(16, 9))
dendrogram(linkage_result, color_threshold=threshold)
plt.show()


# In[12]:


print(clustered, clustered.shape)


# In[13]:


df3 = pd.DataFrame(clustered, columns=['cluster1'])


# In[14]:


df3.head()


# ## k-means使って見た

# In[61]:


X = df1.drop('Unnamed: 0', axis = 1)
km = KMeans(n_clusters=8,
                        init='k-means++',
                        n_init= 10,
                        max_iter=300,
                        #tol=1e-04,#収束と判定するための許容誤差
                        random_state = 0)
y_km = km.fit_predict(X)


# In[62]:


print(y_km, y_km.shape)


# In[63]:


print(km.inertia_)


# In[51]:


df4 = pd.DataFrame(y_km, columns=['kmean4'])


# In[55]:


df7 = pd.DataFrame(y_km, columns=['kmean7'])


# In[64]:


df8 = pd.DataFrame(y_km, columns=['kmean8'])


# In[46]:


X = df1.drop('Unnamed: 0', axis = 1)

distortions = []

for i in range(1, 15):
    km = KMeans(n_clusters=i,
                        init='k-means++',
                        n_init= 10,
                        max_iter=300,
                        #tol=1e-04,#収束と判定するための許容誤差
                        random_state = 0)
    km.fit(X)
    distortions.append(km.inertia_)


# In[47]:


plt.plot(range(1,15), distortions, marker = 'o')
plt.show()


# ## 4,7をとりあえず採用しておく

# In[69]:


df_return = pd.DataFrame(df1['Unnamed: 0']).join([df3]).join([df4]).join([df7]).join([df8])


# In[70]:


df_return = pd.merge(df_return, df, on = 'Unnamed: 0',how='left')


# In[73]:


df_return.to_csv('clu.csv')


# In[72]:


df_return.head()


# In[24]:





# In[75]:


cl = pd.read_csv('Book3.csv')


# In[76]:


df_return = pd.merge(cl, df, on = 'Unnamed: 0',how='left')


# In[77]:


df_return.head()


# In[78]:


df_return.to_csv('add_cluster.csv')


# In[4]:


df = pd.read_csv('add_cluster.csv')


# In[9]:


df.head()


# In[12]:


add_cluster = df.loc[:,['person','cluster_no']]


# In[13]:


add_cluster.to_csv('add_cluster.csv')


# ## クラスター番号を変更する

# In[27]:


add_cluster = pd.read_csv('add_cluster.csv')


# In[34]:


add_cluster.head()


# In[29]:


new_cluster = {i : j for i, j in zip([2,1,3,8,4,6,7,5],[1,2,3,4,5,6,7,8])}


# In[30]:


new_cluster


# In[31]:


add_cluster['new_cluster_no'] = [new_cluster[i] for i in add_cluster['cluster_no'] ]


# In[33]:


add_cluster = add_cluster.loc[:,['person','new_cluster_no']]


# In[35]:


add_cluster.to_csv('add_cluster.csv')


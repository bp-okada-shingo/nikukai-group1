
# coding: utf-8

# In[65]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
plt.rcParams['font.family'] = 'IPAPGothic'
from tqdm import tqdm_notebook as tqdm
import datetime
import seaborn as sns
get_ipython().run_line_magic('matplotlib', 'inline')
import collections
import statistics
import math


# ## マスターの読み込み

# In[2]:


df = pd.read_csv('20180605作成時系列用マスター.csv', parse_dates = ['sakusei_date', 'nyuusya'], index_col=0)


# In[44]:


df.columns


# In[171]:


df.head()


# ## データとクラスターごとにリストを作成

# In[172]:


li_kako_count = []
li_kako_sanka = []
li_kako_match = []
li_kako_zissi = []
for i in range(8):
    li_kako_count.append(df[df['cluster_no']== i+1].pivot_table(values=['kako_count'], index=['sakusei_date'], columns=['id']))
    li_kako_sanka.append(df[df['cluster_no']== i+1].pivot_table(values=['kako_sanka'], index=['sakusei_date'], columns=['id']))
    li_kako_match.append(df[df['cluster_no']== i+1].pivot_table(values=['kako_match'], index=['sakusei_date'], columns=['id']))
    li_kako_zissi.append(df[df['cluster_no']== i+1].pivot_table(values=['kako_zissi'], index=['sakusei_date'], columns=['id']))


# In[173]:


li_kako_match[0].head()


# ### NaNの補完を行う

# In[181]:


def nan_cover(df):
    #退社後はNaNにしたいので逆から数字を埋める
    k = 0
    for i in range(len(df))[::-1]:
        if math.isnan(df[i]) != True:
            k = i
            j = df[i]
        if k != 0:
            if math.isnan(df[i]):
                df[i] = j            
    return(df)


# In[182]:


for i in tqdm(range(8)):
    li_kako_count[i] = li_kako_count[i].apply(nan_cover)
    li_kako_sanka[i] = li_kako_sanka[i].apply(nan_cover)
    li_kako_match[i] = li_kako_match[i].apply(nan_cover)
    li_kako_zissi[i] = li_kako_zissi[i].apply(nan_cover)


# In[180]:


print(li_kako_match[5])


# In[206]:


for i in range(8):
    li_kako_count[i].plot(title=str(i+1)+'メールの配信数',
            ylim = ([0,140]),
            grid=True,#グリッドの有無
            colormap='Accent',#色の指定
            legend=False,#凡例の有無
            alpha=0.8#透過度
            )

    li_kako_sanka[i].plot(title=str(i+1)+'参加表明数',
            ylim = ([0,140]),
            grid=True,#グリッドの有無
            colormap='Accent',#色の指定
            legend=False,#凡例の有無
            alpha=0.8#透過度
            )

    li_kako_macth[i].plot(title=str(i+1)+'マッチング数',
            ylim = ([0,140]),
            grid=True,#グリッドの有無
            colormap='Accent',#色の指定
            legend=False,#凡例の有無
            alpha=0.8#透過度
            )

    li_kako_zissi[i].plot(title=str(i+1)+'実施数',
            ylim = ([0,140]),
            grid=True,#グリッドの有無
            colormap='Accent',#色の指定
            legend=False,#凡例の有無
            alpha=0.8#透過度
            )


# In[200]:


li_kako_sanka[1].plot(title='クラスター',
        ylim = ([0,110]),
        grid=True,#グリッドの有無
        colormap='Accent',#色の指定
        legend=False,#凡例の有無
        alpha=0.8#透過度
        )


# In[201]:


li_kako_macth[1].plot(title='クラスター',
        ylim = ([0,110]),
        grid=True,#グリッドの有無
        colormap='Accent',#色の指定
        legend=False,#凡例の有無
        alpha=0.8#透過度
        )


# In[202]:


li_kako_zissi[1].plot(title='クラスター',
        ylim = ([0,110]),
        grid=True,#グリッドの有無
        colormap='Accent',#色の指定
        legend=False,#凡例の有無
        alpha=0.8#透過度
        )


# In[13]:


df[df['id'] == '100245070535557235332']


# In[14]:


df[df['sakusei_date'] == '2018-02-19']


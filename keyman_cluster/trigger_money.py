
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
plt.rcParams['font.family'] = 'IPAPGothic'
from tqdm import tqdm_notebook as tqdm
import datetime
import seaborn as sns
get_ipython().run_line_magic('matplotlib', 'inline')
import collections
import statistics
import math


# In[2]:


df = pd.read_csv('20180605作成時系列用マスター.csv', parse_dates = ['sakusei_date', 'nyuusya'], index_col=0)


# In[3]:


df.head()


# In[4]:


li_kako_count = []
li_kako_sanka = []
li_kako_match = []
li_kako_zissi = []
for i in range(8):
    li_kako_count.append(df[df['cluster_no']== i+1].pivot_table(values=['kako_count'], index=['sakusei_date'], columns=['id']))
    li_kako_sanka.append(df[df['cluster_no']== i+1].pivot_table(values=['kako_sanka'], index=['sakusei_date'], columns=['id']))
    li_kako_match.append(df[df['cluster_no']== i+1].pivot_table(values=['kako_match'], index=['sakusei_date'], columns=['id']))
    li_kako_zissi.append(df[df['cluster_no']== i+1].pivot_table(values=['kako_zissi'], index=['sakusei_date'], columns=['id']))


# # 金額に着目してみる

# ## 6000 or 12000の割合

# ### 3000円が入ると嫌なので、3000円の支給期間を断定する

# In[71]:


print(
    df[df['shikyuu']== 3000]['sakusei_date'].min(),
    df[df['shikyuu']== 3000]['sakusei_date'].max())


# ### 肉会が開催されてない期間がある

# In[72]:


df.head(2880).tail(100)


# ### 肉会が通常になってからをとりだす

# In[73]:


df1 = df[2865:]


# ### 【全体】肉会の金額による配信数、実施数、実施率、参加表明数、参加表明率

# In[127]:


#何人
df1['id'][~df1['id'].duplicated()].count()


# In[111]:


#6,000円
print(
    'メール配信数：',df1[df1['shikyuu']== 6000]['id'].count(),
    '\n参加表明数：', df1[df1['shikyuu']== 6000][df1['status'] == 1]['id'].count(),
    '\n参加表明率：', (df1[df1['shikyuu']== 6000][df1['status'] == 1]['id'].count())/(df1[df1['shikyuu']== 6000]['id'].count()),
    '\n実施数：',df1[df1['shikyuu']== 6000][df1['zissi_flag'] == True]['id'].count(),
    '\n実施率：', (df1[df1['shikyuu']== 6000][df1['zissi_flag'] == True]['id'].count()/3)/(df1[df1['shikyuu']== 6000]['id'].count()/3)
)


# In[112]:


#12,000円
print(
    'メール配信数：',df1[df1['shikyuu']== 12000]['id'].count(),
    '\n参加表明数：',df1[df1['shikyuu']== 12000][df1['status'] == 1]['id'].count(),
    '\n参加表明率：',(df1[df1['shikyuu']== 12000][df1['status'] == 1]['id'].count())/(df1[df1['shikyuu']== 12000]['id'].count()),
    '\n実施数：', df1[df1['shikyuu']== 12000][df1['zissi_flag'] == True]['id'].count(),
    '\n実施率：',(df1[df1['shikyuu']== 12000][df1['zissi_flag'] == True]['id'].count()/3)/(df1[df1['shikyuu']== 12000]['id'].count()/3)
)


# ### 【クラスター46】肉会の金額による配信数、実施数、実施率、参加表明数、参加表明率

# In[113]:


df46 = df1[df1['cluster_no'].isin([4, 6])]


# In[114]:


#何人いるの
df46['id'][~df46['id'].duplicated()].count()


# In[115]:


#6,000円
print(
    'メール配信数：',df46[df46['shikyuu']== 6000]['id'].count(),
    '\n参加表明数：', df46[df46['shikyuu']== 6000][df46['status'] == 1]['id'].count(),
    '\n参加表明率：', (df46[df46['shikyuu']== 6000][df46['status'] == 1]['id'].count()/3)/(df46[df46['shikyuu']== 6000]['id'].count()/3),
    '\n実施数：',df46[df46['shikyuu']== 6000][df46['zissi_flag'] == True]['id'].count(),
    '\n実施率：', (df46[df46['shikyuu']== 6000][df46['zissi_flag'] == True]['id'].count()/3)/(df46[df46['shikyuu']== 6000]['id'].count()/3)
)


# In[116]:


#12,000円
print(
    'メール配信数：',df46[df46['shikyuu']== 12000]['id'].count(),
    '\n参加表明数：', df46[df46['shikyuu']== 12000][df46['status'] == 1]['id'].count(),
    '\n参加表明率：', (df46[df46['shikyuu']== 12000][df46['status'] == 1]['id'].count()/3)/(df46[df46['shikyuu']== 12000]['id'].count()/3),
    '\n実施数：',df46[df46['shikyuu']== 12000][df46['zissi_flag'] == True]['id'].count(),
    '\n実施率：', (df46[df46['shikyuu']== 12000][df46['zissi_flag'] == True]['id'].count()/3)/(df46[df46['shikyuu']== 12000]['id'].count()/3)
)


# ### 【クラスター4】肉会の金額による配信数、実施数、実施率、参加表明数、参加表明率

# In[117]:


df4 = df1[df1['cluster_no'].isin([4])]


# In[126]:


#何人
df4['id'][~df4['id'].duplicated()].count()


# In[118]:


#6,000円
print(
    'メール配信数：',df4[df4['shikyuu']== 6000]['id'].count(),
    '\n参加表明数：', df4[df4['shikyuu']== 6000][df4['status'] == 1]['id'].count(),
    '\n参加表明率：', (df4[df4['shikyuu']== 6000][df4['status'] == 1]['id'].count()/3)/(df4[df4['shikyuu']== 6000]['id'].count()/3),
    '\n実施数：',df4[df4['shikyuu']== 6000][df4['zissi_flag'] == True]['id'].count(),
    '\n実施率：', (df4[df4['shikyuu']== 6000][df4['zissi_flag'] == True]['id'].count()/3)/(df4[df4['shikyuu']== 6000]['id'].count()/3)
)


# In[119]:


#12,000円
print(
    'メール配信数：',df4[df4['shikyuu']== 12000]['id'].count(),
    '\n参加表明数：', df4[df4['shikyuu']== 12000][df4['status'] == 1]['id'].count(),
    '\n参加表明率：', (df4[df4['shikyuu']== 12000][df4['status'] == 1]['id'].count()/3)/(df4[df4['shikyuu']== 12000]['id'].count()/3),
    '\n実施数：',df4[df4['shikyuu']== 12000][df4['zissi_flag'] == True]['id'].count(),
    '\n実施率：', (df4[df4['shikyuu']== 12000][df4['zissi_flag'] == True]['id'].count()/3)/(df4[df4['shikyuu']== 12000]['id'].count()/3)
)


# ### 【クラスター6】肉会の金額による配信数、実施数、実施率、参加表明数、参加表明率

# In[120]:


df6 = df1[df1['cluster_no'].isin([6])]


# In[121]:


#何人
df6['id'][~df6['id'].duplicated()].count()


# In[122]:


#6,000円
print(
    'メール配信数：',df6[df6['shikyuu']== 6000]['id'].count(),
    '\n参加表明数：', df6[df6['shikyuu']== 6000][df6['status'] == 1]['id'].count(),
    '\n参加表明率：', (df6[df6['shikyuu']== 6000][df6['status'] == 1]['id'].count()/3)/(df6[df6['shikyuu']== 6000]['id'].count()/3),
    '\n実施数：',df6[df6['shikyuu']== 6000][df6['zissi_flag'] == True]['id'].count(),
    '\n実施率：', (df6[df6['shikyuu']== 6000][df6['zissi_flag'] == True]['id'].count()/3)/(df6[df6['shikyuu']== 6000]['id'].count()/3)
)


# In[123]:


#12,000円
print(
    'メール配信数：',df6[df6['shikyuu']== 12000]['id'].count(),
    '\n参加表明数：', df6[df6['shikyuu']== 12000][df6['status'] == 1]['id'].count(),
    '\n参加表明率：', (df6[df6['shikyuu']== 12000][df6['status'] == 1]['id'].count()/3)/(df6[df6['shikyuu']== 12000]['id'].count()/3),
    '\n実施数：',df6[df6['shikyuu']== 12000][df6['zissi_flag'] == True]['id'].count(),
    '\n実施率：', (df6[df6['shikyuu']== 12000][df6['zissi_flag'] == True]['id'].count()/3)/(df6[df6['shikyuu']== 12000]['id'].count()/3)
)


# ### 【クラスター12】肉会の金額による配信数、実施数、実施率、参加表明数、参加表明率

# In[128]:


df12 = df1[df1['cluster_no'].isin([1,2])]


# In[129]:


#何人
df12['id'][~df12['id'].duplicated()].count()


# In[132]:


#6,000円
print(
    'メール配信数：',df12[df12['shikyuu']== 6000]['id'].count(),
    '\n参加表明数：', df12[df12['shikyuu']== 6000][df12['status'] == 1]['id'].count(),
    '\n参加表明率：', (df12[df12['shikyuu']== 6000][df12['status'] == 1]['id'].count())/(df12[df12['shikyuu']== 6000]['id'].count()),
    '\n実施数：',df12[df12['shikyuu']== 6000][df12['zissi_flag'] == True]['id'].count(),
    '\n実施率：', (df12[df12['shikyuu']== 6000][df12['zissi_flag'] == True]['id'].count()/3)/(df12[df12['shikyuu']== 6000]['id'].count()/3)
)


# In[133]:


#12,000円
print(
    'メール配信数：',df12[df12['shikyuu']== 12000]['id'].count(),
    '\n参加表明数：', df12[df12['shikyuu']== 12000][df12['status'] == 1]['id'].count(),
    '\n参加表明率：', (df12[df12['shikyuu']== 12000][df12['status'] == 1]['id'].count())/(df12[df12['shikyuu']== 12000]['id'].count()),
    '\n実施数：',df12[df12['shikyuu']== 12000][df12['zissi_flag'] == True]['id'].count(),
    '\n実施率：', (df12[df12['shikyuu']== 12000][df12['zissi_flag'] == True]['id'].count()/3)/(df12[df12['shikyuu']== 12000]['id'].count()/3)
)


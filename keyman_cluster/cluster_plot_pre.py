
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm_notebook as tqdm
import datetime
import seaborn as sns
import collections
import statistics


# In[5]:


df = pd.read_csv('201805311600_確率*10_20_30倍+スコアつき（元）.csv', index_col=0)


# ## ユーザーマスターを復元する

# In[7]:


df.columns


# In[20]:


df_first = df.loc[:, df.columns.str.contains('first')]
df_second = df.loc[:, df.columns.str.contains('second')]
df_third = df.loc[:, df.columns.str.contains('third')]


# In[30]:


df_all = df.iloc[:, 1:16]
df_all = df_all.drop(['zissi_date', 'zero_lunch_one_dinner', 'shikkou_date', 'shiharaisya_id', 'reply_deadline', 'kingaku', 'match_date'], axis = 1)


# In[31]:


df_all.head()


# In[35]:


df_first = pd.concat([df_all, df_first], axis=1)
df_second = pd.concat([df_all, df_second], axis=1)
df_third = pd.concat([df_all, df_third], axis=1)


# In[40]:


df_first.columns = ['index', 'nikukai_id', 'shikyuu', 'sakusei_date', 'haishin_flag',
       'zissi_flag', 'match_flag', 'revenge_flag', 'id', 'status',
       'syozoku_1', 'syozoku_2', 'syozoku_3', 'sei',
       'nyuusya', 'yakusyoku', 'zaiseki', 'shinsotsu',
       'kako_count', 'kako_sanka', 'kako_match',
       'kako_zissi', 'keika', 'syozoku_kakou',
       'yakusyoku_kakou']
df_second.columns = ['index', 'nikukai_id', 'shikyuu', 'sakusei_date', 'haishin_flag',
       'zissi_flag', 'match_flag', 'revenge_flag', 'id', 'status',
       'syozoku_1', 'syozoku_2', 'syozoku_3', 'sei',
       'nyuusya', 'yakusyoku', 'zaiseki', 'shinsotsu',
       'kako_count', 'kako_sanka', 'kako_match',
       'kako_zissi', 'keika', 'syozoku_kakou',
       'yakusyoku_kakou']
df_third.columns = ['index', 'nikukai_id', 'shikyuu', 'sakusei_date', 'haishin_flag',
       'zissi_flag', 'match_flag', 'revenge_flag', 'id', 'status',
       'syozoku_1', 'syozoku_2', 'syozoku_3', 'sei',
       'nyuusya', 'yakusyoku', 'zaiseki', 'shinsotsu',
       'kako_count', 'kako_sanka', 'kako_match',
       'kako_zissi', 'keika', 'syozoku_kakou',
       'yakusyoku_kakou']


# ## くっつける

# In[41]:


df_new = pd.concat([df_first, df_second, df_third], ignore_index=True)


# In[43]:


df_new.head()


# ## そーとする

# In[51]:


df_new = df_new.sort_values(by='sakusei_date', ascending=True)


# In[52]:


df_new.head()


# In[53]:


df_new.to_csv('20180605作成時系列用マスター.csv')


# In[2]:


df_master = pd.read_csv('20180605作成時系列用マスター.csv', parse_dates = ['nyuusya'], index_col=0)


# In[3]:


df_master['sakusei_date'] = [ i[0:10] for i in df_master['sakusei_date']]


# In[4]:


df_master['sakusei_date'] = [datetime.datetime.strptime(i, '%Y-%m-%d') for i in df_master['sakusei_date']]


# In[5]:


df_master.head()


# In[6]:


df_master= df_master.reset_index(drop=True)


# ## クラスター番号をつける

# In[7]:


df_user = pd.read_csv('20180604_member.csv', parse_dates = ['nyuusya'], index_col=0)


# In[8]:


df_user = df_user.loc[:, ['id', 'cluster_no']]


# In[9]:


df_new = pd.merge(df_master, df_user, how='left')


# In[10]:


df_new.tail(30)


# In[18]:


df_new.to_csv('20180605作成時系列用マスター.csv')


# ## クラスターごとに分析してみる

# In[11]:


df7 = df_new[df_new['cluster_no']== 7]


# In[12]:


df7.head()


# In[16]:


df7.pivot_table(values=['kako_count'], index=['sakusei_date'], columns=['id'])#, aggfunc='sum')#, fill_value=0)


# In[17]:


df_new[df_new['id'] == '100230077931556158975']


# In[107]:


df_new[df_new['sakusei_date'] == '2018-01-04']


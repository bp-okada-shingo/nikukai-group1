
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
plt.rcParams['font.family'] = 'IPAPGothic'
from tqdm import tqdm_notebook as tqdm
import datetime
import seaborn as sns
get_ipython().run_line_magic('matplotlib', 'inline')
import collections
import statistics
import math


# In[2]:


df_time = pd.read_csv('20180605作成時系列用マスター.csv', parse_dates = ['sakusei_date', 'nyuusya'], index_col=0)


# In[3]:


df_user = pd.read_csv('20180604_member.csv', parse_dates = ['nyuusya'],index_col=0)
df_user.shape


# In[4]:


df_score = pd.read_csv('201805311600_確率*10_20_30倍+スコアつき（元）.csv' ,index_col=0)
df_score = df_score.loc[:, ['nikukai_id', 'first', 'second', 'third']]


# # 人に注目してみる

# In[6]:


df46 = df_time[df_time['cluster_no'].isin([4, 6])]
df4 = df_time[df_time['cluster_no'].isin([4])]
df6 = df_time[df_time['cluster_no'].isin([6])]
df12 = df_time[df_time['cluster_no'].isin([1, 2])]


# In[7]:


def person(df_time, df_user, df_score, cluster_no):
    #cluster_noは [4,6] のようにリスト型
    #flag は 'status'のように書く
    
    #クラスターを持ってくる
    df0 = df_time[df_time['cluster_no'].isin(cluster_no)]
    
    #フラグで持ってきた人の肉会IDとID
    df1 = pd.DataFrame(df0.loc[:, ['nikukai_id', 'id']])
   
    #肉会IDの抽出をし、そして肉会の詳細をくっつける
    df0 = df0['nikukai_id'][~df0['nikukai_id'].duplicated()]
    df0 = pd.merge(pd.DataFrame(df0), df_score, how = 'left')
    
    #上記の肉会IDに紐づく肉会ID、IDをまとめる
    df11 = df0.loc[:,['nikukai_id', 'first']]
    df11.columns=['nikukai_id','id']
    
    df22 = df0.loc[:,['nikukai_id', 'second']]
    df22.columns=['nikukai_id','id']
    
    df33 = df0.loc[:,['nikukai_id', 'third']]
    df33.columns=['nikukai_id','id']
    
    df = df11.append(df22, ignore_index=True)
    df = df.append(df33, ignore_index=True)
    
    #左側にクラスターで引き抜いた人をくっつける
    
    
    #df0とdf1をくっつけてグルーピングし、合計を出す
    df = pd.concat([df, df1])
    df = df.groupby(['id']).sum().reset_index(drop=False)
    
    #idを元にその人の情報をくっつけ,回数を基準にそーとする
    df = pd.merge(df, df_user, how = 'left')
    df = df.sort_values(by='count', ascending=False).reset_index(drop=True)
    
    return(df)
    


# In[8]:


person(df_time, df_user, df_score, [4,6])


# In[31]:


status1_46 = person(df_time, df_user, df_score, [4,6], 'status')
status1_4 = person(df_time, df_user, df_score, [4], 'status')
status1_6 = person(df_time, df_user, df_score, [6], 'status')
status1_12 = person(df_time, df_user, df_score, [1,2], 'status')


# In[35]:


zissi_46 = person(df_time, df_user, df_score, [4,6], 'zissi_flag')
zissi_4 = person(df_time, df_user, df_score, [4], 'zissi_flag')
zissi_6 = person(df_time, df_user, df_score, [6], 'zissi_flag')
zissi_12 = person(df_time, df_user, df_score, [1,2], 'zissi_flag')


# In[37]:


zissi_12.head(20)


# In[43]:


data = [status1_46, status1_4, status1_6, status1_12, zissi_46, zissi_4, zissi_6, zissi_12]
label = ['status1_46', 'status1_4', 'status1_6', 'status1_12', 'zissi_46', 'zissi_4', 'zissi_6', 'zissi_12']


# In[49]:


for i in range(8):
    print(
        label[i],
        '\n',
        '上位２０人',
        data[i][:20]['id'].count(),
        '\n',
        '役職持ち',
        data[i][:20][data[i]['yakusyoku_kakou'] == 'その他']['id'].count(),
        '\n',
        'その割合',
        data[i][:20][data[i]['yakusyoku_kakou'] == 'その他']['id'].count()/data[i][:20]['id'].count()
)


# # 人気者を探す

# In[19]:


#データの抜き出し
df12 = df_score.loc[:,['nikukai_id', 'first', 'second']]
df13 = df_score.loc[:,['nikukai_id','first', 'third']]
df21 = df_score.loc[:,['nikukai_id', 'second', 'first']]
df23 = df_score.loc[:,['nikukai_id', 'second', 'third']]
df31 = df_score.loc[:,['nikukai_id', 'third', 'first']]
df32 = df_score.loc[:,['nikukai_id', 'third', 'second']]
#カラム名の変更
df12.columns=['nikukai_id','id0','id']
df13.columns=['nikukai_id','id0','id']
df21.columns=['nikukai_id','id0','id']
df23.columns=['nikukai_id','id0','id']
df31.columns=['nikukai_id','id0','id']
df32.columns=['nikukai_id','id0','id']
#dfまとめる
df = df12
df = df.append(df13)
df = df.append(df21)
df = df.append(df23)
df = df.append(df31)
df = df.append(df32)

#肉会IDとIDをキーにしてdf_timeをくっつける
df = pd.merge(df, df_time, on= ['nikukai_id', 'id'])

#必要な列だけ抜き出す
df = df.loc[:, [ 'id0', 'haishin_flag', 'zissi_flag', 'match_flag','status' , 'cluster_no']]

#status=2を０に置き換える
df.loc[df['status']== 2] = 0

#True=1, False=0に置き換える
def tf(cell):
    if cell == True:
        return(1)
    elif cell == False:
        return(0)
    else:
        return(cell)

df = df.applymap(tf)

#id0とclustor_noでグループピングし、合計を出す
df_result = df.groupby(['id0','cluster_no']).sum()
df_result = df_result.reset_index(drop=False)

#実施率と参加表明され率の算出
df_result['par_zissi'] = [i for i in df_result['zissi_flag']/df_result['haishin_flag']]
df_result['par_status1'] = [i for i in df_result['status']/df_result['haishin_flag']]

#id0に詳細情報をくっつける
df_result.columns = ['id', 'pair_cluster_no', 'haishin_flag', 'zissi_flag', 'match_flag', 'status', 'par_zissi', 'par_status1']
df_result = pd.merge(df_result, df_user, how='left', on= ['id'])

#index=0を削除する
df_result = df_result.drop(0)


# In[24]:


#中央値を算出してみる
zissi_median = [df_result[df_result['pair_cluster_no']== i+1]['par_zissi'].median() for i in range(8)]
status1_median = [df_result[df_result['pair_cluster_no']== i+1]['par_status1'].median() for i in range(8)]


# In[25]:


zissi_median


# In[26]:


status1_median


# In[33]:


df_result['zissi_mean'] = [zissi_median[i-1] for i in df_result['pair_cluster_no']]
df_result['zissi_status1'] = [status1_median[i-1] for i in df_result['pair_cluster_no']]


# In[34]:


df_result.head()


# In[80]:


df_result[df_result['pair_cluster_no']== 4].sort_values(by='par_status1', ascending=False)


# In[35]:


df_result.to_csv('trigger.csv')


# In[58]:


df_result[df_result['pair_cluster_no']== 6].plot(kind='scatter',
                                                 x='par_zissi', 
                                                 y='par_status1',
                                                 c = 'cluster_no',
                                                 cmap='rainbow',
                                                 title = '実施率と参加表明率'
                                                )


# In[88]:


sns.lmplot(x='par_zissi',
           y='par_status1',
           data=df_result[df_result['pair_cluster_no']== 6][df_result['haishin_flag'] > 10],
           hue = 'cluster_no',
           fit_reg=False)
plt.ylabel('出席表明率')
plt.xlabel('実施率')
plt.title('クラスター６が肉会一緒に行きたい人')
plt.show()


# In[89]:


sns.lmplot(x='par_zissi',
           y='par_status1',
           data=df_result[df_result['pair_cluster_no']== 6][df_result['haishin_flag'] > 10],
           hue = 'syozoku_1',
           fit_reg=False)
plt.ylabel('出席表明率')
plt.xlabel('実施率')
plt.title('クラスター６が肉会一緒に行きたい人')
plt.show()


# In[90]:


sns.lmplot(x='par_zissi',
           y='par_status1',
           data=df_result[df_result['pair_cluster_no']== 6][df_result['haishin_flag'] > 10],
           hue = 'yakusyoku',
           fit_reg=False)
plt.ylabel('出席表明率')
plt.xlabel('実施率')
plt.title('クラスター６が肉会一緒に行きたい人')
plt.show()

